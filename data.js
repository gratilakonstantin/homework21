class Hamburger {
    constructor(size, stuffing) {

        this.size = size;

        this.stuffing = stuffing;

        this.toppings = [];

    }

    static SIZE_SMALL = {price: 50, calories: 20};

    static SIZE_LARGE = {price: 100, calories: 40};

    static STUFFING_CHEESE = {price: 10, calories: 20};

    static STUFFING_SALAD = {price: 20, calories: 5};

    static STUFFING_POTATO = {price: 15, calories: 10};

    static TOPPING_SPICE = {price: 15, calories: 0};

    static TOPPING_MAYO = {price: 20, calories: 5};


    addTopping(topping) {
        this.toppings.push(topping);
    }

    calculatePrice() {

        let price = 0;

        price += this.size.price;

        price += this.stuffing.price;

        this.toppings.forEach((topping) => {
            price += topping.price;
        });
        return price;
    }

    calculateCalories() {

        let calories = 0;
        calories += this.size.calories;

        calories += this.stuffing.calories;

        this.toppings.forEach((topping) => {
            calories += topping.calories;
        });
        return calories;
    }

}

console.log()
function handleSubmit(event) {

    event.preventDefault();

    const size = document.querySelector('input[name="size"]:checked').value;

    const stuffingList = document.querySelectorAll('input[name="stuffing"]:checked');

    const toppingsList = document.querySelectorAll('input[name="topping"]:checked');

    const stuffing = [];

    stuffingList.forEach((item) => {

        stuffing.push(item.value);

    });

    const toppings = [];

    toppingsList.forEach((item) => {

        toppings.push(item.value);

    });


    const hamburger = new Hamburger(size === 'small' ? Hamburger.SIZE_SMALL : Hamburger.SIZE_LARGE,

        stuffing.includes('cheese') ? Hamburger.STUFFING_CHEESE :

            stuffing.includes('salad') ? Hamburger.STUFFING_SALAD :

                stuffing.includes('potato') ? Hamburger.STUFFING_POTATO : null);


    toppings.forEach((topping) => {

        hamburger.addTopping(topping === 'spice' ? Hamburger.TOPPING_SPICE : Hamburger.TOPPING_MAYO);

    });


    const price = hamburger.calculatePrice();

    const calories = hamburger.calculateCalories();


    const resultElement = document.getElementById('result');

    resultElement.innerHTML = `Price
: ${price}
    tugriks, Calories
: ${calories}`

}


const submitButton = document.getElementById('calculateBtn');

submitButton.addEventListener('click', handleSubmit);


